FROM ubuntu:bionic
RUN ln -snf /bin/bash /bin/sh
SHELL ["/bin/bash", "--login", "-c"]

COPY files/Miniconda3-4.7.12.1-Linux-x86_64.sh /var/tmp/miniconda.sh
RUN chmod +x /var/tmp/miniconda.sh && /var/tmp/miniconda.sh -b -p /opt/miniconda
RUN /opt/miniconda/bin/conda config --add channels conda-forge
RUN /opt/miniconda/bin/conda update -n base conda

ENV BASH_ENV "/opt/miniconda/etc/profile.d/conda.sh"
